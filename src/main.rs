use std::{fs::File, io::{Write, Read}, env};

mod images;

const GRID_SIZE: i32 = 100;
const STROKE_WIDTH: i32 = 20;
const DOT_RADIUS: i32 = STROKE_WIDTH;
const STROKE: &str = "white";



#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    InvalidArgument,
    UnreachableError,
    InvalidFormat,
    TooLittleArguments
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error::Io(err)
    }
}



#[derive(Debug, Clone)]
pub enum Direction {
    DownRight = 0b00,
    UpRight = 0b01,
    DownLeft = 0b10,
    UpLeft = 0b11,
}

impl Direction {
    fn from_byte(byte: u8) -> Result<Self, Error> {
        if byte == 0b00 {
            return Ok(Self::DownRight)
        } else if byte == 0b01 {
            return Ok(Self::UpRight);
        } else if byte == 0b10 {
            return Ok(Self::DownLeft);
        } else if byte == 0b11 {
            return Ok(Self::UpLeft);
        }
        Err(Error::InvalidArgument)
    }

    fn get_x_multiplier(self) -> i32 {
        (-2 * ((self as i32 & 0b10) >> 1)) + 1
    }

    fn get_y_multiplier(self) -> i32 {
        (-2 * (self as i32 & 0b1)) + 1
    }
}

#[derive(Debug, Clone)]
pub enum Move {
    NewPosition(i8, i8),
    StraightLine(i8, i8),
    ArcClockwise(u8, Direction),
    ArcAntiClockwise(u8, Direction),
}

impl Move {
    fn as_byte(self) -> u8 {
        match self {
            Move::NewPosition(x, y) => {
                let xu = x as u8;
                let yu = y as u8;
                (0b00 << 6) | ((xu & 0b111) << 3) | (yu & 0b111)
            },
            Move::StraightLine(x, y) => {
                let xu = x as u8;
                let yu = y as u8;
                (0b01 << 6) | ((xu & 0b111) << 3) | (yu & 0b111)
            },
            Move::ArcClockwise(n, d) => {
                (0b10 << 6) | ((n & 0b1111) << 2) | (d as u8 & 0b11)
            },
            Move::ArcAntiClockwise(n, d) => {
                (0b11 << 6) | ((n & 0b1111) << 2) | (d as u8 & 0b11)
            }
        }
    }

    fn from_byte(byte: u8) -> Result<Move, Error> {
        let k = (byte & 0b11000000) >> 6;
        let x = (byte & 0b00111000) >> 3;
        let y = (byte & 0b00000111) >> 0;
        let n = (byte & 0b00111100) >> 2;
        let d = Direction::from_byte(byte & 0b00000011)?;

        let xi = i3(x);
        let yi = i3(y);

        if k == 0b00 {
            return Ok(Move::NewPosition(xi, yi));
        } else if k == 0b01 {
            return Ok(Move::StraightLine(xi, yi));
        } else if k == 0b10 {
            return Ok(Move::ArcClockwise(n, d));
        } else if k == 0b11 {
            return Ok(Move::ArcAntiClockwise(n, d));
        }
        Err(Error::UnreachableError)
    }
}



#[derive(Debug, Clone)]
pub struct Image {
    w: u8,
    h: u8,
    moves: Vec<Move>
}

impl Image {
    pub fn create(w: u8, h: u8, moves: Vec<Move>) -> Image {
        Image{ w, h, moves }
    }

    pub fn from_bytes(bytes: Vec<u8>, debug_print: bool) -> Result<Image, Error> {
        if bytes.len() < 2 {
            return Err(Error::InvalidFormat);
        }

        let mut moves = Vec::<Move>::new();
        for byte in &bytes[2..bytes.len()]{
            let m = Move::from_byte(*byte)?;
            if debug_print {
                println!("Loading move from byte {}: {:?}", byte, &m);
            }
            moves.push(m);
        }

        Ok(Image::create(bytes[0], bytes[1], moves))
    }

    pub fn as_bytes(self) -> Vec<u8> {
        let mut result = vec![self.w, self.h];
        for m in self.moves {
            result.push(m.as_byte());
        }
        result
    }

    pub fn as_svg(self) -> String {
        let mut svg_code = format!("<svg width=\"{}\" height=\"{}\">",
            (self.w as i32 * GRID_SIZE) + (2 * STROKE_WIDTH),
            (self.h as i32 * GRID_SIZE) + (2 * STROKE_WIDTH)
        );

        let mut path_command = String::new();
        let mut cur_x = 0;
        let mut cur_y = 0;
        let mut is_dot = false;
        let mut dot_x = 0;
        let mut dot_y = 0;
        let mut new_position_at_beginning = false;



        fn new_svg_obj(svg_code: &mut String, path_command: &mut String, is_dot: &mut bool, dot_x: i32, dot_y: i32) {
            if *is_dot {
                *svg_code += &format!(
                    "<circle cx=\"{}\" cy=\"{}\" r=\"{}\" stroke=\"black\" stroke-width=\"0\" fill=\"{}\" />",
                    dot_x,
                    dot_y,
                    DOT_RADIUS,
                    STROKE
                );
                *is_dot = false;
            } else {
                *svg_code += &format!(
                    "<path d=\"{}\" stroke=\"{}\" stroke-width=\"{}\" fill=\"none\" />",
                    path_command,
                    STROKE,
                    STROKE_WIDTH
                );
                *path_command = String::new();
            }
        }



        for m in self.moves {

            // Fix for files that begin with a (0, 0) position but without the NewPosition(0, 0) move at the beginning
            match m {
                Move::NewPosition(_, _) => {
                    new_position_at_beginning = true;
                },
                _ => {
                    if !new_position_at_beginning {
                        path_command += &format!("M{},{} ", map_grid_coord(cur_x), map_grid_coord(cur_y));
                    }
                }
            }

            match m {
                Move::NewPosition(x, y) => {
                    if path_command != "" {
                        // Save the previous path to the svg
                        new_svg_obj(&mut svg_code, &mut path_command, &mut is_dot, dot_x, dot_y);
                    }

                    cur_x += x as i32;
                    cur_y += y as i32;
                    path_command += &format!("M{},{} ", map_grid_coord(cur_x), map_grid_coord(cur_y));
                },
                Move::StraightLine(x, y) => {
                    cur_x += x as i32;
                    cur_y += y as i32;
                    if x == 0 && y == 0 {
                        is_dot = true;
                        dot_x = map_grid_coord(cur_x);
                        dot_y = map_grid_coord(cur_y);
                    } else {
                        path_command += &format!("L{},{} ", map_grid_coord(cur_x), map_grid_coord(cur_y));
                    }
                },
                Move::ArcClockwise(n, d) => {
                    let r = n as i32;
                    cur_x += r * d.clone().get_x_multiplier();
                    cur_y += r * d.get_y_multiplier();
                    path_command += &format!("A{},{} 0 0,1 {},{} ", map_grid_size(r), map_grid_size(r), map_grid_coord(cur_x), map_grid_coord(cur_y));
                },
                Move::ArcAntiClockwise(n, d) => {
                    let r = n as i32;
                    cur_x += r * d.clone().get_x_multiplier();
                    cur_y += r * d.get_y_multiplier();
                    path_command += &format!("A{},{} 0 0,0 {},{} ", map_grid_size(r), map_grid_size(r), map_grid_coord(cur_x), map_grid_coord(cur_y));
                }
            }
        }

        // Save the last path to the svg
    new_svg_obj(&mut svg_code, &mut path_command, &mut is_dot, dot_x, dot_y);

        svg_code += "</svg>";
        svg_code
    }
}



fn i3(x: u8) -> i8 {
    return (x & 0b11) as i8 + (-4 * (x >> 2) as i8);
}

fn map_grid_coord(x: i32) -> i32 {
    (x * GRID_SIZE) + STROKE_WIDTH
}

fn map_grid_size(x: i32) -> i32 {
    x * GRID_SIZE
}



fn load_bytes(filename: String) -> std::io::Result<Vec<u8>> {
    let mut file = File::open(filename)?;
    let mut bytes = Vec::<u8>::new();
    file.read_to_end(&mut bytes)?;
    Ok(bytes)
}

fn save_bytes(bytes: Vec<u8>, filename: String) -> std::io::Result<()> {
    let mut file = File::create(filename)?;
    file.write_all(&bytes)?;
    Ok(())
}

fn save_text(text: String, filename: String) -> std::io::Result<()> {
    let mut file = File::create(filename)?;
    file.write_all(text.as_bytes())?;
    Ok(())
}



fn export_images() -> Result<(), Error> {
    let all_images = images::get_all_images();
    
    std::fs::create_dir_all("generated/sgi")?;
    std::fs::create_dir_all("generated/svg")?;

    for entry in all_images {
        println!("Exporting image: {:?}", entry);

        let key = entry.0;
        let image = entry.1;

        let sgi_filename = String::from(format!("generated/sgi/{}.sgi", key));
        let svg_filename = String::from(format!("generated/svg/{}.svg", key));

        let bytes = image.as_bytes();
        save_bytes(bytes.clone(), sgi_filename)?;

        let image = Image::from_bytes(bytes, false)?;
        let svg: String = image.as_svg();
        save_text(svg, svg_filename)?;
    }
    Ok(())
}

fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        println!("Generating built-in images");
        return export_images();
    }

    if args.len() < 3 {
        println!("Error: Too little arguments");
        println!("Usage: {} <input_file> <output_file>", args[0]);
        return Err(Error::TooLittleArguments);
    }

    let bytes = load_bytes(args[1].clone())?;
    let image = Image::from_bytes(bytes, true)?;
    println!("Loaded image: {:?}", image);

    let svg: String = Image::as_svg(image);
    save_text(svg, args[2].clone())?;
    Ok(())
}
